window.addEventListener("load", async () => {
  const { data } = (await getData()) ?? {};
  if (data && data.error) setError(data);
});

async function getData() {
  return fetch("/error.json").then((res) => res.json());
}

function setError(error) {
  const xMark = `<svg class="icon" id="x-mark" xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M24 20.188l-8.315-8.209 8.2-8.282-3.697-3.697-8.212 8.318-8.31-8.203-3.666 3.666 8.321 8.24-8.206 8.313 3.666 3.666 8.237-8.318 8.285 8.203z"/></svg>`;
  const bubble = document.querySelector("#status");
  const desc = document.querySelector("#desc");

  const eta = "\n" + (error?.eta ? "ETA: " + error.eta : "No ETA");

  desc.innerText = (error?.message ?? "") + eta;
  desc.innerHTML =
    '<span class="underline">http://getip.pk</span> is currently down!<br />' +
    desc.innerHTML;
  bubble.innerHTML = xMark;
  bubble.classList.add("error");
}
